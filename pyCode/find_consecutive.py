# In this code we need to find the number of consecutives number that adds up to form the given number
import math as m

def cal_Consecutive(num):
    result = []
    counter = m.ceil(num/2)  # half + 1
    start = 1
    while(start < counter):  # from start(1) is less then half + 1
        sum = 0
        for i in range(start, counter + 1):  # every time values will change as start var is incrementing
            sum += i  # calculating sum
            if sum == num:  # if sum is equal to desired num
                tupl = [] 
                for j in range(start, i + 1):  # adding element in list, when number consecutively added it will for the desired number
                    tupl.append(j)
                result.append(tuple(tupl))  # adding list as a tuple in result list
                break
            if sum > num: 
                break
        start += 1
    return result

print(cal_Consecutive(125))