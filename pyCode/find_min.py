# In this code we need we do following things
#     1) find the smallest number
#     2) subtract that number from the other numbers
#     3) if because of the above operation, some number becomes zero then remove them
#     4) count the number of elements after every (2) operation and append the result in the result list
import copy

def find_nums(l):
    count = 1
    result = []
    result.append(len(l))  # adding count before operation
    while len(l) > 0:
        min_val = min(l)  # finding min value
        l = [(item - min_val)  for item in l]  # subtracting min_val from list's each element 
        l2 = copy.deepcopy(l)
        for item in l:  # removing zero numbers
            if item <= 0:
                l2.remove(item)
        l = l2
        if len(l) > 0:
            result.append(len(l))  # appending elements count in result list
        count +=1
    return result

print(find_nums([6,5,4,4,2,2,8]))
